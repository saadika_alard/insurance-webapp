(function() {
  'use strict';

  angular
    .module('insuranceWebapp')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
