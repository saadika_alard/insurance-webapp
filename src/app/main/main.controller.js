(function() {
  'use strict';

  angular
    .module('insuranceWebapp')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, $http, $scope) {
    var vm = this;

// Calling the current date
    $scope.date = new Date();

    $scope.policyResponses = [];

    $scope.policyDetail = [];

    $scope.selectedClient = null;

    $scope.selectedPolicy = '';

// Calling multiple APIs

      $http.get(
          'http://41.193.214.130:2510/eiftidemo/CLT_LIST')
          .then(function success(response) {

            $scope.clientData = response;
            response.data.clients.forEach(function(client) {
              var url = 'http://41.193.214.130:2510/eiftidemo/POL_LIST?client='+client.client;
              $http.get(url)
              .then(function success(policyResponse){
                $scope.policyResponses.push(policyResponse.data);

                $scope.policyResponses.forEach(function(policies){
                  for (var i = 0; i < $scope.policyResponses.length; i++) {
                    $scope.policyResponses[i].policies.forEach(function(policy) {
                        var url = 'http://41.193.214.130:2510/eiftidemo/POL_DET?policy='+ policy.policyNumber +'&client='+client.client;
                        $http.get(url)
                        .then(function success(policyDetails){
                          $scope.policyDetail.push(policyDetails);
                        }, function error(policyDetails){
                          console.log(policyDetails, 'Ooops something went wrong!!!!!');
                        });
                    })
                  }


                })
              }, function error(policyResponse){
                console.log('Something went wrong!!!!!!');
              })

            });
          },function error(response) {
            console.log('Error!!!!!!');
          });

          $scope.SelectClient = function(client, id) {
            $scope.selectedClient = client;
            var detailBlock = document.getElementById('policyDetails' + id);
            // if(detailBlock.style.display === 'table-cell'){
            //   detailBlock.style.display = 'none';
            // } else {
              detailBlock.style.display = 'table-cell';
            // }
          }

          $scope.SelectClientPolicy = function(policy, id) {
            $scope.selectedClientPolicy = policy;
            var policyBlock = document.getElementById('clientPolicy' + id);
            // if(detailBlock.style.display === 'table-cell'){
            //   detailBlock.style.display = 'none';
            // } else {
              policyBlock.style.display = 'table-cell';
            // }

            console.log(selectedClientPolicy);
          }

          // modal function



  }
})();
