/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('insuranceWebapp')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
